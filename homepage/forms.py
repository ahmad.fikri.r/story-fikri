from django.forms import ModelForm
from .models import Matkul,Kegiatan1,Kegiatan2,Kegiatan3

class matkulForm(ModelForm):

    class Meta:
        model = Matkul
        fields = '__all__'

class formPeserta1(ModelForm):

    class Meta:
        model = Kegiatan1
        fields = '__all__'

class formPeserta2(ModelForm):

    class Meta:
        model = Kegiatan2
        fields = '__all__'

class formPeserta3(ModelForm):

    class Meta:
        model = Kegiatan3
        fields = '__all__'
