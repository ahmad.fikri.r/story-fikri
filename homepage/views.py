from django.shortcuts import render
from .forms import matkulForm,formPeserta1,formPeserta2,formPeserta3
from .models import Matkul,Kegiatan1,Kegiatan2,Kegiatan3
from django.shortcuts import redirect

# Create your views here.

def first(request):
    return render(request, 'first.html')

def gallery(request):
    return render(request, 'gallery.html')

def riwayat(request):
    return render(request, 'riwayat.html')

def socialmedia(request):
    return render(request, 'socialmedia.html')

def story3(request):
    return render(request, 'story3.html')
    
def matkulforms(request):
    form = matkulForm()
    matakul = Matkul.objects.all()
    if request.method == 'POST':
        form = matkulForm(request.POST)
        matakul = Matkul.objects.all()

        if form.is_valid():
            form.save()

    context = {'form':form, 'matakul':matakul}
    return render(request, 'form.html', context)

def matkulpage(request):
    form = matkulForm()
    matakul = Matkul.objects.all()
    if request.method == 'POST':
        form = matkulForm(request.POST)

        if form.is_valid():
            form.save()
    context = {'form':form, 'matakul':matakul}
    return render(request, 'matkul.html', context)
    
def deletematkul(request,pk):
    form = matkulForm()
    matakul = Matkul.objects.all()
    if request.method == 'POST':
        form = matkulForm(request.POST)

        if form.is_valid():
            form.save()
    else:
        Matkul.objects.filter(pk=pk).delete()
        data = Matkul.objects.all()
        form = matkulForm()        

    context = {'form':form, 'matakul':matakul}
    return render(request, 'matkul.html', context)   

def kegiatan(request):
    if request.method == 'POST' and 'btn_form1' in request.POST:
        form_peserta1 = formPeserta1(request.POST)
        if form_peserta1.is_valid():
            form_peserta1.save()
    elif request.method == 'POST' and 'btn_form2' in request.POST:
        form_peserta2 = formPeserta2(request.POST)
        if form_peserta2.is_valid():
            form_peserta2.save()
    elif request.method == 'POST' and 'btn_form3' in request.POST:
        form_peserta3 = formPeserta3(request.POST)
        if form_peserta3.is_valid():
            form_peserta3.save()

    form_peserta1 = formPeserta1()
    form_peserta2 = formPeserta2()
    form_peserta3 = formPeserta3()
    kegiatan_1 = Kegiatan1.objects.all()
    kegiatan_2 = Kegiatan2.objects.all()
    kegiatan_3 = Kegiatan3.objects.all()
    context = {
        'list_kegiatan_1' : kegiatan_1,
        'list_kegiatan_2' : kegiatan_2,
        'list_kegiatan_3' : kegiatan_3,
        'form_peserta1' : form_peserta1,
        'form_peserta2' : form_peserta2,
        'form_peserta3' : form_peserta3,
    }
    return render(request, 'kegiatan.html', context) 


def formkegiatan(request):
    form = formPeserta()
    kegiatanpes = Kegiatan.objects.all()
    if request.method == 'POST':
        form = formPeserta(request.POST)
        kegiatanpes = Kegiatan.objects.all()

        if form.is_valid():
            form.save()
    context = {'form':form, 'kegiatanpes':kegiatanpes}
    return render(request, 'formkegiatan.html', context)

