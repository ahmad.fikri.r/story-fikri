from django.test import TestCase
from django.urls import reverse
from .models import Kegiatan1,Kegiatan2,Kegiatan3
from .forms import formPeserta1,formPeserta2,formPeserta3

class UnitTest(TestCase):
    def test_root_url_status_301(self):
        response = self.client.get(reverse('homepage:kegiatan'))
        self.assertEqual(response.status_code, 200)

    def test_template_yang_digunakan_dari_halaman_kegiatan(self):
        response = self.client.get(reverse('homepage:kegiatan'))
        self.assertTemplateUsed(response, 'kegiatan.html')

    def test_models_dari_halaman_kegiatan(self):
        Kegiatan1.objects.create(nama='abc')
        n_futsal = Kegiatan1.objects.all().count()
        self.assertEqual(n_futsal,1)
        Kegiatan2.objects.create(nama='abc')
        n_atletik = Kegiatan2.objects.all().count()
        self.assertEqual(n_atletik,1)
        Kegiatan3.objects.create(nama='abc')
        n_tinju = Kegiatan3.objects.all().count()
        self.assertEqual(n_tinju,1)

    def test_form_futsal_post_valid(self):
        response_post = self.client.post('/kegiatan', {'nama':'abc', 'btn_form1': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,301)
        form_futsal = formPeserta1(data={'nama':"abc"})
        self.assertTrue(form_futsal.is_valid())
        self.assertEqual(form_futsal.cleaned_data['nama'],"abc")
    
    def test_form_coding_post_valid(self):
        response_post = self.client.post('/kegiatan', {'nama':'abc', 'btn_form2': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,301)
        form_atletik = formPeserta2(data={'nama':"abc"})
        self.assertTrue(form_atletik.is_valid())
        self.assertEqual(form_atletik.cleaned_data['nama'],"abc")

    def test_form_sepeda_post_valid(self):
        response_post = self.client.post('/kegiatan', {'nama':'abc', 'btn_form3': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,301)
        form_tinju = formPeserta3(data={'nama':"abc"})
        self.assertTrue(form_tinju.is_valid())
        self.assertEqual(form_tinju.cleaned_data['nama'],"abc")