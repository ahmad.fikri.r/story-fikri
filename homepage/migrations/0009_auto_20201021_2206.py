# Generated by Django 3.1.2 on 2020-10-21 15:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0008_kegiatan'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='kegiatan',
            name='nama',
        ),
        migrations.AddField(
            model_name='kegiatan',
            name='ragunan',
            field=models.CharField(default='', max_length=50),
        ),
    ]
