from django.urls import path
from . import views
from django.conf.urls import include

app_name = 'homepage'

urlpatterns = [
    path('', views.first, name='first'),
    path('story3/', views.story3, name='story3'),
    path('gallery/', views.gallery, name='gallery'),
    path('riwayat/', views.riwayat, name='riwayat'),
    path('socialmedia/', views.socialmedia, name='socialmedia'),
    path('isimatkul/', views.matkulforms, name='matkulforms'),
    path('matkul/', views.matkulpage, name='matkulpage'),
    path('<int:pk>', views.deletematkul, name='delete'),
    path('kegiatan/',views.kegiatan, name='kegiatan'),
    path('formkegiatan/',views.formkegiatan, name='formkegiatan'),

    
    # dilanjutkan ...
]