from django.db import models

# Create your models here.

class Matkul(models.Model):
    matakuliah = models.CharField(max_length=50,default='')
    dosen = models.CharField(max_length=100,default='')
    sks = models.IntegerField()
    deskripsi = models.CharField(max_length=50,default='')
    tahun = models.CharField(max_length=50,default='')
    kelas = models.CharField(max_length=50,default='')

class Kegiatan1(models.Model):
    nama = models.CharField(max_length=50,default='')

class Kegiatan2(models.Model):
    nama = models.CharField(max_length=50,default='')

class Kegiatan3(models.Model):
    nama = models.CharField(max_length=50,default='')